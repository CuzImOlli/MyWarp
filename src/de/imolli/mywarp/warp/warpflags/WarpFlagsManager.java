package de.imolli.mywarp.warp.warpflags;

import de.imolli.mywarp.MyWarp;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class WarpFlagsManager {

    private static File file = new File("plugins/MyWarp/warpflags.yml");
    private static YamlConfiguration config;

    public static void init() {

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                MyWarp.getPlugin().getLogger().log(Level.SEVERE, "An error occurred while creating 'warpflags.yml'!");
            }
        }

        config = YamlConfiguration.loadConfiguration(file);

        loadConfig();
        loadWarpFlagsDefaults();

    }

    private static void loadConfig() {

        config.options().header("WarpFlags of MyWarp \n\n" +
                "Please note that editing the configurations while the server is running is not recommended.\n\n" +
                "\n" +
                "Default:\n" +
                "   Set which warpflags are by default enabled. \n\n");
        config.options().copyDefaults(true);

        for (WarpFlag warpFlag : WarpFlag.values()) {

            config.addDefault("Default." + warpFlag.getName(), false);

        }

        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void loadWarpFlagsDefaults() {

        for (String key : config.getKeys(true)) {
            if (key.startsWith("Default.")) {
                String keyw = key.replace("Default.", "");
                System.out.println(WarpFlag.values().toString() + " WARPFLAGUS: " + keyw);
                for (WarpFlag w : WarpFlag.values()) {
                    if (w.getName().equalsIgnoreCase(keyw)) {
                        System.out.println("Hallo" + w.getName() + " " + keyw);
                        w.setDefault(config.getBoolean(key));
                    }
                }
            }
        }

    }

}
